from django.conf import settings
from django.urls import path, include, re_path
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static


urlpatterns = [
    path('', include('schemas.urls')),
    re_path(r'^celery-progress/', include('celery_progress.urls')),

    path('login/',
         auth_views.LoginView.as_view(template_name='login.html'),
         name='login'),

    path('logout/',
         auth_views.LogoutView.as_view(),
         name='logout'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


handler404 = 'schemas.front.views.handler404'
handler500 = 'schemas.front.views.handler500'
