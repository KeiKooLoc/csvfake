from __future__ import annotations
from pathlib import Path
from uuid import uuid4
# External
import numpy as np
from celery.utils.log import get_task_logger
# Typing
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from typing import Any, List, Optional
    from pathlib import PosixPath
    from celery_progress.backend import ProgressRecorder
    from .structures import UnionColumns


logger = get_task_logger(__name__)


_CSV_DIR = Path(__file__).parent / 'csv_dir'
_CSV_DIR.mkdir(parents=True, exist_ok=True)


class CsvBuilder:
    """
    Generating csv file.
    """
    def __init__(self, name, separator, dc_array):

        self._name: str = name
        self._separator: str = separator
        self._dc_array: List[UnionColumns] = dc_array

        # csv file header.
        self._header: str = self._separator.join(
            [dc.header for dc in self._dc_array]
        )
        # csv file columns format.
        self._fmt: List[str] = [dc.fmt for dc in self._dc_array]

        # File name.
        self._filename: str = f'{self._name}-{uuid4()}.csv'
        # Full path where file will be saved.
        self._filepath: PosixPath = _CSV_DIR / self._filename

        # Items to create on one poll interval.
        self._burst: int = 3000
        # Object for handling celery progress.
        self._progress: Optional[_Progress] = None

    def create(self,
               rows: int,
               progress_recorder: Optional[ProgressRecorder] = None):
        """
        Generate csv file, save it to directory and return file path.
        """
        if progress_recorder:
            # Create object for handling celery progress
            total_bursts = (len([_ for _ in range(0, rows, self._burst)])
                            * len(self._dc_array))
            self._progress = _Progress(progress_recorder=progress_recorder,
                                       total_bursts=total_bursts)

        output = np.zeros(rows, dtype=[dc.dtype for dc in self._dc_array])
        for dc in self._dc_array:
            column_data = self._generate_column(dc, rows)
            output[dc.header] = column_data

        np.savetxt(fname=self._filepath,
                   X=output,
                   fmt=self._fmt,
                   header=self._header,
                   delimiter=self._separator,
                   comments='')
        return self._filepath

    def _generate_column(self, dc: UnionColumns, rows: int) -> List[Any]:
        """
        Generate list with column items.
        """
        logger.info(f"dc: {dc}")
        column_items = list()
        for step in range(0, rows, self._burst):
            if rows <= self._burst:
                burst = rows
            elif rows - step < self._burst:
                burst = rows - step
            else:
                burst = self._burst

            column_items += dc.values(burst)
            if self._progress:
                logger.info(f'\nrows={rows}'
                            f'\nburst={burst}'
                            f'\nstep={step}')
                self._progress.update()
        return column_items


class _Progress:
    """
    Handle csv files creation progress
    and update data for celery-result progress_recorder.
    """
    def __init__(self,
                 progress_recorder: ProgressRecorder,
                 total_bursts: int):

        self._progress_recorder = progress_recorder
        self._total_bursts = total_bursts
        self._current_burst: int = 0

    def update(self):
        self._current_burst += 1
        logger.info(f'\ntotal_bursts={self._total_bursts}'
                    f'\ncurrent_burst={self._current_burst}')
        percent = 100 * float(self._current_burst) / float(self._total_bursts)
        self._progress_recorder.set_progress(int(percent), 100)
