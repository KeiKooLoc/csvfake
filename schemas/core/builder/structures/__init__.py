from .structures import *
from .settings import SchemaSetting
from typing import Union, List


# All csv columns types.
structures = [
    Name,
    Integer,
    Text,
    Job,
    Email,
    Domain,
    Phone,
    Company,
    Address,
    Date
]

# Typing
UnionColumns = Union[
    Name,
    Integer,
    Text,
    Job,
    Email,
    Domain,
    Phone,
    Company,
    Address,
    Date
]

ListColumns = List[UnionColumns]

# Settings for schemas.
SCHEMA = SchemaSetting(structures)
