from __future__ import annotations
from dataclasses import dataclass
# External
import numpy as np
# Internal
from .settings import SchemaSetting
# Typing
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from typing import Union, Tuple


@dataclass
class _BaseType:

    # Column header.
    header: str
    # Column is not ranged by default.
    is_ranged = False


@dataclass
class _StringType(_BaseType):

    # String separator
    character: str = SchemaSetting.CHARACTER_CHOICES[0][0]
    # Numpy type.
    np_type: object = object
    # Numpy type format.
    fmt: str = '%s'

    @property
    def dtype(self) -> Tuple:
        return self.header, self.np_type

    def _format(self, value: str) -> str:
        return SchemaSetting.CHARACTER_FORMS[self.character].format(value)


@dataclass
class _IntegerType(_BaseType):

    np_type: np.int64 = np.int64
    fmt: str = '%d'

    @property
    def dtype(self) -> Tuple:
        return self.header, self.np_type
