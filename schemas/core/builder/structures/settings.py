
class SchemaSetting:

    # First item is default.
    SEPARATOR_CHOICES = (
        (',', 'Comma(,)'),
        ('_', 'Underscore(_)'),
        ('-', 'Dash(-)'),
        ('*', 'Star(*)'),
        ('.', 'Dot(.)')
    )

    # First item is default.
    CHARACTER_CHOICES = (
        ('"', 'Double-quote(")'),
        ('\'', 'Single-quote(\')'),
        ('"""', 'Triple(double)-quote(""")'),
        ("'''", "Triple(single)-quote(''')"),
    )

    CHARACTER_FORMS = {
        character: character + '{}' + character
        for character, _ in CHARACTER_CHOICES
    }

    def __init__(self, structures):

        self.COLUMN_TYPES = {
            type_.ID: type_ for type_ in structures
        }

        self.COLUMN_CHOICES = [
            (type_.ID, type_.name) for type_ in structures
        ]

        self.RANGED_TYPES = [
            type_.ID for type_ in structures if type_.is_ranged
        ]

        self.RANGED_TYPES_DICT = {
            type_.ID: type_ for type_ in structures if type_.is_ranged
        }

        self.COLUMN_TYPES = {
            type_.ID: type_ for type_ in structures
        }
