# -*- coding: utf-8 -*-
from __future__ import annotations
import random
from dataclasses import dataclass
# External
import faker
from faker_e164.providers import E164Provider
import faker.exceptions
# Internal
from .base import _StringType, _IntegerType
# Typing
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from typing import List, Optional, Tuple


@dataclass
class Integer(_IntegerType):

    # String to save in database.
    ID: str = 'integer'
    # Frontend name.
    name: str = 'Integer'
    # If column type is ranged need to set it True.
    is_ranged: bool = True
    # If column type is ranged - set validation data for min and max fields.
    # If None - mean no validation.
    min: Optional[int] = None
    max: Optional[int] = None

    def values(self, count: int = 1) -> List[int]:
        r = self._range()
        return [random.randint(*r) for _ in range(count)]

    def _range(self) -> Tuple[int, int]:
        return (self.min if self.min else -1_000_000,
                self.max if self.max else 1_000_000)


@dataclass
class Text(_StringType):

    ID: str = "text"
    name: str = "Text"

    is_ranged: bool = True
    min: int = 1
    max: int = 300

    def values(self, count: int = 1) -> List[str]:
        fake = faker.Faker()
        randint = random.randint(self.min, self.max)
        return [self._format(fake.paragraph(nb_sentences=randint))
                for _ in range(count)]


@dataclass
class Name(_StringType):

    ID: str = 'name'
    name: str = 'Full Name'

    def values(self, count: int = 1) -> List[str]:
        fake = faker.Faker()
        return [self._format(fake.first_name() + ' ' + fake.last_name())
                for _ in range(count)]


@dataclass
class Job(_StringType):
    ID: str = "job"
    name: str = "Job"

    def values(self, count: int = 1) -> List[str]:
        fake = faker.Faker()
        return [self._format(fake.job()) for _ in range(count)]


@dataclass
class Email(_StringType):

    ID: str = "email"
    name: str = "Email"

    def values(self, count: int = 1) -> List[str]:
        fake = faker.Faker()
        return [self._format(fake.email()) for _ in range(count)]


@dataclass
class Domain(_StringType):

    ID: str = "domain"
    name: str = "Domain name"

    def values(self, count: int = 1) -> List[str]:
        fake = faker.Faker()
        return [self._format(fake.domain_name()) for _ in range(count)]


@dataclass
class Phone(_StringType):

    ID: str = "phone"
    name: str = "Phone number"

    def values(self, count: int = 1) -> List[str]:
        fake = faker.Faker()
        fake.add_provider(E164Provider)
        return [self._format(fake.e164(valid=False)) for _ in range(count)]


@dataclass
class Company(_StringType):

    ID: str = "company"
    name: str = "Company name"

    def values(self, count: int = 1) -> List[str]:
        fake = faker.Faker()
        return [self._format(fake.company()) for _ in range(count)]


@dataclass
class Address(_StringType):

    ID: str = "address"
    name: str = "Address"

    def values(self, count: int = 1) -> List[str]:
        fake = faker.Faker()
        return [self._format(fake.address()) for _ in range(count)]


class Date(_StringType):

    ID: str = "date"
    name: str = "Date"

    def values(self, count: int = 1) -> List[str]:
        fake = faker.Faker()
        return [self._format(fake.date()) for _ in range(count)]
