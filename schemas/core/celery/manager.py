# -*- coding: utf-8 -*-
from __future__ import annotations
# External
from celery.result import GroupResult
from django.contrib.auth import get_user_model
# Internal
from .tasks import create_csv
from ..models import Task
# Typing
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from typing import Dict, List


class TaskManager:
    """
    Handle celery tasks.
    """

    @classmethod
    def run_tasks(cls, user_id: int, rows: int):
        """
        Run tasks for creation csv files from user schemas without csv file.
        And save task ids.
        """
        group_id = cls._group_id(user_id)
        group = cls._get_group(group_id)
        schemas_ids = _TaskCache.get_schemas_ids(group_id)
        schemas = (
            get_user_model()
            .objects.get(pk=user_id)
            .schema_set.filter(csv_file='')
            .exclude(id__in=schemas_ids)
        )
        for schema in schemas:
            task = create_csv.delay(schema_id=schema.id, rows=rows)
            group.add(task)
            group.save()
            _TaskCache.create(task_id=task.task_id,
                              schema_id=schema.id,
                              group_id=group_id)

    @classmethod
    def get_ids(cls, user_id: int) -> Dict[str, int]:
        """
        Get all active and reserved task ids with schemas ids.
        """
        group_id = cls._group_id(user_id)
        task_ids = [result.id for result in cls._get_group(group_id)]
        result = _TaskCache.get_ids(task_ids)
        return result

    @classmethod
    def _get_group(cls, group_id: str) -> GroupResult:
        """
        Get group or create new if doesn't exist.
        """
        group = GroupResult.restore(group_id)
        if not group:
            group = GroupResult(id=group_id, results=list())
        cls._clear_group(group)
        return group

    @classmethod
    def _clear_group(cls, group: GroupResult):
        """
        Remove all 'ready' tasks from the group and database.
        """
        to_delete = list()
        for result in group.results:
            if result.ready():
                group.discard(result)
                group.save()
                to_delete.append(result.id)
        _TaskCache.delete(to_delete)

    @staticmethod
    def _group_id(user_id: int) -> str:
        return str(user_id)


class _TaskCache:
    """
    Handle list of reserved and active tasks with schemas ids.

    Generally I don't why CELERY_RESULT_EXTENDED doesn't works
    and how to get task args and kwargs from AsyncResult object in other way.
    So I just save it to db.
    """

    @staticmethod
    def create(task_id: str, schema_id: int, group_id: str):
        task = Task(task_id=task_id, schema_id=schema_id, group_id=group_id)
        task.save()

    @staticmethod
    def delete(task_ids: List[str]):
        Task.objects.filter(task_id__in=task_ids).delete()

    @staticmethod
    def get_schemas_ids(group_id: str) -> List[int]:
        tasks = Task.objects.filter(group_id=group_id)
        return [task.schema_id for task in tasks]

    @staticmethod
    def get_ids(task_ids: List[str]) -> Dict[str, int]:
        return {
            task.task_id: task.schema_id
            for task in Task.objects.filter(task_id__in=task_ids)
        }
