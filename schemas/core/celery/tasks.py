# -*- coding: utf-8 -*-
from __future__ import annotations
import os
# External
from celery import shared_task
from celery_progress.backend import ProgressRecorder
from django.urls import reverse
from django.core.files import File
from celery.utils.log import get_task_logger
# Internal
from ..models import Schema


logger = get_task_logger(__name__)


@shared_task(bind=True, task_track_started=True)
def create_csv(self, schema_id: int, rows: int):
    logger.info(f'Start csv generation task for schema with id: {schema_id}.')
    schema = Schema.objects.get(pk=schema_id)
    progress_recorder = ProgressRecorder(self)
    csv_factory = schema.builder()
    file_path = csv_factory.create(rows=rows,
                                   progress_recorder=progress_recorder)
    # Save file for schema.
    with open(file_path, 'rb') as f:
        file = File(f)
        schema.csv_file.save(f'{schema.id}.csv', file)
    # Remove file from prev directory.
    os.remove(file_path)
    return reverse('schemas:download', args=[schema.id])
