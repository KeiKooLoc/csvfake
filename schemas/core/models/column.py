# -*- coding: utf-8 -*-
from __future__ import annotations
# External
from django.db import models
# Internal
from ..builder import SCHEMA
from .schema import Schema
# Typing
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from ..builder.structures import UnionColumns


class Column(models.Model):

    schema = models.ForeignKey(Schema, on_delete=models.CASCADE)
    name = models.CharField(max_length=400)
    type = models.CharField(max_length=400,
                            choices=SCHEMA.COLUMN_CHOICES,
                            default=SCHEMA.COLUMN_CHOICES[0][0])
    min = models.IntegerField(null=True)
    max = models.IntegerField(null=True)
    order = models.IntegerField(null=True, blank=True)

    class Meta:
        ordering = ['order']

    def __str__(self):
        return f'Column: {self.name}'

    def to_dataclass(self) -> UnionColumns:
        """
        Convert column to dataclass for building csv file.
        """
        dc = SCHEMA.COLUMN_TYPES.get(self.type)
        if dc:
            args = dict(header=self.name)
            dc_fields = dc.__dataclass_fields__
            if 'character' in dc_fields:
                args['character'] = self.schema.str_character

            if 'min' in dc_fields and 'max' in dc_fields:
                if self.min:
                    args['min'] = self.min
                if self.max:
                    args['max'] = self.max
            dc = dc(**args)
        return dc
