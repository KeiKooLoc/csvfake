# -*- coding: utf-8 -*-
from __future__ import annotations
# External
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.dispatch import receiver
# Internal
from ..builder import SCHEMA, CsvBuilder
# Typing
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from typing import List
    from ..builder.structures import UnionColumns


class Schema(models.Model):

    name = models.CharField(max_length=400)
    separator = models.CharField(max_length=10,
                                 choices=SCHEMA.SEPARATOR_CHOICES,
                                 default=SCHEMA.SEPARATOR_CHOICES[0][0])
    str_character = models.CharField(max_length=10,
                                     choices=SCHEMA.CHARACTER_CHOICES,
                                     default=SCHEMA.CHARACTER_CHOICES[0][0])
    timestamp = models.DateTimeField('timestamp', auto_now_add=True)
    csv_file = models.FileField(null=True)
    user = models.ForeignKey(get_user_model(),
                             on_delete=models.CASCADE)

    # Need to buy pycharm pro version and remove next line.
    objects = models.Manager()

    class Meta:
        ordering = ['-timestamp']

    def __str__(self):
        return f'Schema: {self.name}'

    def builder(self) -> CsvBuilder:
        """
        Ready to use csv file builder object for schema.
        """
        return CsvBuilder(name=self.name,
                          separator=self.separator,
                          dc_array=self._dc_array())

    def _dc_array(self) -> List[UnionColumns]:
        """
        Create list of column dataclasses for building csv file.
        """
        return [cln.to_dataclass() for cln in self.column_set.all()]


@receiver(models.signals.post_delete, sender=Schema)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes csv file from filesystem
    when corresponding `Schema` object is deleted.
    """
    instance.csv_file.delete(save=False)
