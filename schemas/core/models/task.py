from django.db import models


class Task(models.Model):
    """
    Table for current reserved and active tasks.
    todo: remove table and find way to get schema id from AsyncResult object.
    """
    task_id = models.CharField(max_length=1000)
    schema_id = models.IntegerField()
    group_id = models.TextField()  # Same as admin user id but in str type.

    # Need to buy pycharm pro version and remove next line.
    objects = models.Manager()

    def __str__(self):
        return f'{self.task_id} ({self.schema_id})'
