# External
from django import forms
# Internal
from ...core.models import Column
from ...core.builder import SCHEMA


_FORM_INPUT_ATTR = {
    'class': 'form-control'
}


class ColumnForm(forms.ModelForm):

    type_field_attrs = {
        "onChange": 'addFields(this, ''"{{ ranged_types }}")'
    }

    name = forms.CharField(label='Column name',
                           required=True,
                           max_length=300)

    type = forms.ChoiceField(label='Column type',
                             required=True,
                             initial=SCHEMA.COLUMN_CHOICES[0][0],
                             choices=SCHEMA.COLUMN_CHOICES,
                             widget=forms.Select(attrs=type_field_attrs))

    min = forms.IntegerField(label='From',
                             required=False,
                             initial=1)

    max = forms.IntegerField(label='To',
                             required=False,
                             initial=1)

    class Meta:
        model = Column
        fields = ['name', 'type', 'min', 'max', 'order',
                  'schema', 'id']
        widgets = {
            'schema': forms.HiddenInput(),
            'is_ranged': forms.HiddenInput(),
            'order': forms.HiddenInput()
        }

    def __init__(self, *args, **kwargs):
        super(ColumnForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update(_FORM_INPUT_ATTR)
        self.fields['type'].widget.attrs.update(_FORM_INPUT_ATTR)
        self.fields['min'].widget.attrs.update(_FORM_INPUT_ATTR)
        self.fields['max'].widget.attrs.update(_FORM_INPUT_ATTR)


