# External
from django import forms
from django.core.exceptions import ValidationError
from django.template.loader import render_to_string
# Internal
from .column import ColumnForm
from ...core.models import Schema, Column
from ...core.builder import SCHEMA


_FORM_INPUT_ATTR = {
    'class': 'form-control'
}


class ColumnFormSet(forms.BaseInlineFormSet):

    @property
    def templated_empty_form(self) -> str:
        """
        Return html template for empty form.
        """
        context = {'form': self.empty_form,
                   'prefix': self.prefix}
        content = render_to_string('column_form.html', context)
        return content

    def get_ordering_widget(self):
        return forms.NumberInput(attrs=_FORM_INPUT_ATTR)

    def clean(self):
        """
        Validation for columns formset.
        """

        if any(self.errors):
            return

        if len(self.forms) == len(self.deleted_forms):
            raise ValidationError('At least one column required.')

        titles = dict()
        for form in self.forms:

            if self.can_delete and self._should_delete_form(form):
                continue

            if not form.cleaned_data.get('name'):
                msg = "This field is required."
                form.add_error('name', msg)

            if form.cleaned_data.get('ORDER') is None:
                msg = "This field is required."
                form.add_error('ORDER', msg)

            # Validate that no two columns have the same name.
            title = form.cleaned_data.get('name')
            if title and title in titles:
                msg = 'Columns must have distinct names.'
                form.add_error('name', msg)
                titles[title].add_error('name', msg)
            titles[title] = form

            # Validate 'min' and 'max' fields for selected column types.
            selected_type = form.cleaned_data.get('type')
            ranged_type = SCHEMA.RANGED_TYPES_DICT.get(selected_type)

            selected_min = form.cleaned_data.get('min')
            selected_max = form.cleaned_data.get('max')

            # Check if 'min' field is required.
            if ranged_type and selected_min is None:
                msg = "This field is required."
                form.add_error('min', msg)

            # Check if 'max' field is required.
            if ranged_type and selected_max is None:
                msg = "This field is required."
                form.add_error('max', msg)

            # Check if 'min' is smaller that 'max'.
            if (selected_min is not None
                    and selected_max is not None
                    and selected_min > selected_max):
                msg = f"Minimum value can't be grater than maximum."
                form.add_error('min', msg)

            # Check 'min' field for allowed value.
            if (ranged_type
                    and ranged_type.min is not None
                    and selected_min is not None
                    and selected_min < ranged_type.min):
                msg = (f"Minimum value for the {ranged_type.name} " 
                       f"is {ranged_type.min}")
                form.add_error('min', msg)

            # Check 'max' field got allowed value.
            if (ranged_type
                    and ranged_type.max is not None
                    and selected_max is not None
                    and selected_max > ranged_type.max):
                msg = (f"Maximum value for the {ranged_type.name} "
                       f"is {ranged_type.max}")
                form.add_error('max', msg)


column_formset = forms.inlineformset_factory(
    Schema,
    Column,
    min_num=1,
    extra=0,
    can_delete=True,
    can_order=True,
    form=ColumnForm,
    formset=ColumnFormSet
)
