from django import forms


class GenerateForm(forms.Form):
    rows = forms.IntegerField(min_value=1, required=True, initial=1)
