# External
from django import forms
# Internal
from ...core.models import Schema
from ...core.builder import SCHEMA


_FORM_INPUT_ATTR = {
    'class': 'form-control'
}


class SchemaForm(forms.ModelForm):

    name = forms.CharField(label='Name',
                           required=True,
                           max_length=300)

    separator = forms.ChoiceField(label='Column separator',
                                  required=True,
                                  choices=SCHEMA.SEPARATOR_CHOICES)

    str_character = forms.ChoiceField(label='String character',
                                      required=True,
                                      choices=SCHEMA.CHARACTER_CHOICES)

    class Meta:
        model = Schema
        fields = ['name', 'separator', 'str_character', 'id']

    def __init__(self, *args, **kwargs):
        super(SchemaForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update(_FORM_INPUT_ATTR)
        self.fields['separator'].widget.attrs.update(_FORM_INPUT_ATTR)
        self.fields['str_character'].widget.attrs.update(_FORM_INPUT_ATTR)

