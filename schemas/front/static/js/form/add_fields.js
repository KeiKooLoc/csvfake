function addFields (selector) {
    // Hide and show 'min' and 'max' form fields.
    var id = $(selector).attr('id')
    var minMaxBlockSelector = 'div[id=min-max-form-block-' + id + ']';
    if (rangedTypes.includes($(selector).val())) {
        $(minMaxBlockSelector).attr('class', "visible");
    } else {
        $(minMaxBlockSelector).attr('class', "invisible");
    };
};
