function addForm(prefix, emptyForm) {
    // Add form to formset.
    var total = $('#id_' + prefix + '-TOTAL_FORMS').val();
    var ValidForm = emptyForm.replaceAll('__prefix__', total)
    total++;
    $('#id_' + prefix + '-TOTAL_FORMS').val(total);
    $(ValidForm).appendTo('#formset_wrapper');
};
