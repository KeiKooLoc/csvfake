function deleteForm(prefix, selector) {
    // Delete form from formset.
    var total = $('#id_' + prefix + '-TOTAL_FORMS').val();
    if (total > 1) {
        var id = $(selector).attr('id')
        total--;
        $('#id_' + prefix + '-TOTAL_FORMS').val(total);
        $("#column_form_wrapper-" + id).remove();
    };
};
