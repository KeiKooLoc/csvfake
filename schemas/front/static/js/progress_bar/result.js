function customResult(resultElement, result) {
    // Insert download link after task completed.
    var link = $("<a>");
    link.attr("href", result);
    link.text("Download");
    $( resultElement ).append(link);
};
