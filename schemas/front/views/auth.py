# -*- coding: utf-8 -*-
from __future__ import annotations
# External
from django.http import Http404
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
# Typing
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from typing import Union
    from .data_sets import DataSetsListView, GenerateView, DownloadView
    from .schema import (UpdateSchema, DeleteSchema,
                         CreateSchemaView, SchemasList)


class AdminRequiredMixin(LoginRequiredMixin, UserPassesTestMixin):
    """
    Restrict access to not login and not superusers.
    """

    login_url = reverse_lazy('login')

    def test_func(self: 'Union['
                        'CreateSchemaView, '
                        'UpdateSchema, '
                        'DeleteSchema, '
                        'SchemasList, '
                        'DataSetsListView, '
                        'GenerateView, '
                        'DownloadView]'):

        return self.request.user.is_superuser or self.request.user.is_staff


class SchemaPermissionMixin(object):
    """
    Restrict access to schema object.
    """
    def dispatch(self: 'Union[UpdateSchema, DeleteSchema, DownloadView]',
                 request,
                 *args,
                 **kwargs):

        user = self.request.user
        schema = self.get_object()
        if not schema.user == user:
            raise Http404
        return super(SchemaPermissionMixin, self).dispatch(
            request, *args, **kwargs)
