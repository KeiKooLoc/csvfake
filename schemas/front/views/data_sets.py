# External
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.urls import reverse_lazy
from django.views import generic, View
# Internal
from django.views.generic.detail import SingleObjectMixin

from .auth import AdminRequiredMixin, SchemaPermissionMixin
from ..forms import GenerateForm
from ...core.models import Schema
from ...core.celery import task_manager


class DataSetsListView(AdminRequiredMixin,
                       generic.edit.FormMixin,
                       generic.ListView):

    model = Schema
    form_class = GenerateForm
    queryset = Schema.objects.order_by('-timestamp')
    context_object_name = 'data_sets'
    template_name = 'data_sets.html'
    paginate_by = 20

    def get_queryset(self):
        queryset = super(DataSetsListView, self).get_queryset()
        queryset = queryset.filter(user=self.request.user)
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tasks = task_manager.get_ids(user_id=self.request.user.id)
        context['task_ids'] = sorted(tasks.items())
        return context


class GenerateView(AdminRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        """
        Generate csv file for all schemas without csv_file.
        """
        form = GenerateForm(request.GET)
        if form.is_valid():
            rows = form.cleaned_data['rows']
            task_manager.run_tasks(user_id=request.user.id, rows=rows)
        return HttpResponseRedirect(reverse_lazy('schemas:data_sets'))


class DownloadView(AdminRequiredMixin,
                   SingleObjectMixin,
                   SchemaPermissionMixin,
                   View):

    model = Schema

    def get(self, request, *args, **kwargs):
        """
        Download csv file for given schema pk.
        """
        schema = self.get_object()
        if schema.csv_file:
            response = HttpResponse(schema.csv_file,
                                    content_type='text/plain')
            content = "attachment; filename={}".format(schema.csv_file.name)
            response['Content-Disposition'] = content
            return response
        else:
            raise Http404
