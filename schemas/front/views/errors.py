# External
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy


def handler404(request, exception, template_name="errors/404.html"):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse_lazy('login'))
    else:
        response = render(request, template_name)
        response.status_code = 404
    return response


def handler500(request, template_name="errors/500.html"):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse_lazy('login'))
    else:
        response = render(request, template_name)
        response.status_code = 500
    return response
