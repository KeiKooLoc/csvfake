# External
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
# Internal
from .auth import AdminRequiredMixin, SchemaPermissionMixin
from ..forms import SchemaForm, column_formset
from ...core.builder import SCHEMA
from ...core.models import Schema


__all__ = ['CreateSchemaView', 'UpdateSchema', 'SchemasList', 'DeleteSchema']


class CreateSchemaView(AdminRequiredMixin, generic.CreateView):

    model = Schema
    form_class = SchemaForm
    template_name = 'schema.html'
    title = 'New Schema'

    def get_context_data(self, **kwargs):
        context = super(CreateSchemaView, self).get_context_data(**kwargs)
        context['column_formset'] = column_formset(prefix="column")
        context['ranged_types'] = SCHEMA.RANGED_TYPES
        context['title'] = self.title
        return context

    def post(self, request, *args, **kwargs):
        schema_form = SchemaForm(request.POST)
        column_form = column_formset(request.POST, prefix="column")

        if schema_form.is_valid() and column_form.is_valid():
            # Save schema.
            schema = schema_form.save(commit=False)
            schema.user = request.user
            schema.save()
            # Save column.
            column_form.instance = schema
            _set_valid_order(column_form)
            for form in column_form.forms:
                form.instance.order = form.cleaned_data['ORDER']
            column_form.save()
            return HttpResponseRedirect(reverse_lazy('schemas:schemas'))
        else:
            context = {
                'form': schema_form,
                'column_formset': column_form,
                'ranged_types': SCHEMA.RANGED_TYPES,
                'title': self.title
            }
            return render(request, self.template_name, context)


class UpdateSchema(AdminRequiredMixin,
                   SchemaPermissionMixin,
                   generic.edit.UpdateView):

    model = Schema
    form_class = SchemaForm
    template_name = 'schema.html'
    title = 'Update Schema'
    success_url = reverse_lazy('schemas:schemas')

    def get_context_data(self, **kwargs):
        context = super(UpdateSchema, self).get_context_data(**kwargs)
        if self.request.POST:
            context['column_formset'] = column_formset(
                self.request.POST,
                instance=self.object,
                prefix="update_column")
        else:
            context['column_formset'] = column_formset(
                instance=self.object,
                prefix="update_column")

        context['ranged_types'] = SCHEMA.RANGED_TYPES
        context['title'] = self.title
        return context

    def form_valid(self, form, *args, **kwargs):
        context = self.get_context_data()
        column_form = context['column_formset']
        if column_form.is_valid():
            self.object = form.save()
            column_form.instance = self.object
            _set_valid_order(column_form)
            for form in column_form.forms:
                form.instance.order = form.cleaned_data['ORDER']
            column_form.save()
            self.object.csv_file.delete()
            return HttpResponseRedirect(self.get_success_url())
        else:
            context = {
                'form': form,
                'column_formset': column_form,
                'ranged_types': SCHEMA.RANGED_TYPES,
                'title': self.title
            }
            return render(self.request, self.template_name, context)


class SchemasList(AdminRequiredMixin, generic.ListView):

    queryset = Schema.objects.all()
    context_object_name = 'schemas'
    template_name = 'schemas.html'
    paginate_by = 20

    def get_queryset(self):
        queryset = super(SchemasList, self).get_queryset()
        queryset = queryset.filter(user=self.request.user)
        return queryset


class DeleteSchema(AdminRequiredMixin,
                   SchemaPermissionMixin,
                   generic.DeleteView):

    model = Schema
    template_name = 'confirm_delete_schema.html'
    success_url = reverse_lazy('schemas:schemas')


def _set_valid_order(forms):
    """
    In 'ORDER' field inputs can be random integers,
    so set correct order for columns.
    """
    forms = [form for form in forms
             if form.cleaned_data.get('ORDER') is not None]
    sorted_forms = sorted(forms, key=lambda k: k.cleaned_data.get('ORDER'))
    for order, form in enumerate(sorted_forms):
        form.cleaned_data['ORDER'] = order
