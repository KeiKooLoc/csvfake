from django.urls import path

from .front import views

app_name = "schemas"


urlpatterns = [
    path('', views.SchemasList.as_view(), name='schemas'),
    path('schemas/', views.SchemasList.as_view(), name='schemas'),
    path('create/', views.CreateSchemaView.as_view(), name='create'),
    path('<int:pk>/edit', views.UpdateSchema.as_view(), name='edit'),
    path('<int:pk>/delete/', views.DeleteSchema.as_view(), name='delete'),

    path('data_sets/', views.DataSetsListView.as_view(), name='data_sets'),
    path('generate_files/', views.GenerateView.as_view(), name='generate'),
    path('<int:pk>/download/', views.DownloadView.as_view(), name='download'),
]
